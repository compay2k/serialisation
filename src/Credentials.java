import java.io.Serializable;

public class Credentials {

	private String login;
	private transient String password;
	
	
	public Credentials(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}


	@Override
	public String toString() {
		return "Credentials [login=" + login + ", password=" + password + "]";
	}
	
	
}
