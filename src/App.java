import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class App {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Personne p = new Personne(3526, "Hendrix", "Jimi");
		
		Credentials c = new Credentials("jhendrix", "FoxyLady");
		
		p.setCredentials(c);
		
		FileOutputStream fos = new FileOutputStream("c:\\toto\\personne.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(p);
		
		FileInputStream fis = new FileInputStream("c:\\toto\\personne.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		Personne p2 = (Personne)ois.readObject();

		System.out.println(p2);
	}

}
