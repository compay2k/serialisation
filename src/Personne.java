import java.io.Serializable;

public class Personne implements Serializable {

	private int id;
	private String nom;
	private String prenom;
	private transient Credentials credentials;
	
	
	public Credentials getCredentials() {
		return credentials;
	}

	public void setCredentials(Credentials credentials) {
		this.credentials = credentials;
	}

	public Personne() {}
	
	public Personne(int id, String nom, String prenom) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", credentials : " + credentials + "]";
	}
	
	
	
	
}
